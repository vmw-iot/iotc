#!/opt/myiot/venv/bin/python

import logging
import platform
from rpi import vcgencmd
from pulse.settings import APP_ID, RPI_GW0_ID
from pulse.client import open_session, close_session, send_properties


def get_board_ver():
    board_ver = 'Unknown'
    with open('/sys/firmware/devicetree/base/model', 'r') as f:
        board_ver = f.read()

    return board_ver[:-1]


def get_board_sn():
    board_sn = 'xxx'
    with open('/sys/firmware/devicetree/base/serial-number', 'r') as f:
        board_sn = f.read()

    return board_sn[:-1]


def get_camera_installed():
    camera = vcgencmd(['get_camera'])

    if camera[-1:] == '1':
        return 'Yes'
    else:
        return 'No'


def get_arm_mem():
    mem = vcgencmd(['get_mem', 'arm'])
    return mem[4:]


def get_gpu_mem():
    mem = vcgencmd(['get_mem', 'gpu'])
    return mem[4:]


def main():
    session = open_session(APP_ID)

    properties = {
        'board-version': get_board_ver(),
        'platform': platform.platform(),
        'board-serial': get_board_sn(),
        'board-arm-mem': get_arm_mem(),
        'board-gpu-mem': get_gpu_mem(),
        'camera-installed': get_camera_installed()
    }

    result = send_properties(session, RPI_GW0_ID, properties)

    logging.info(f'Sent properties with result = {result}, {properties}')
    close_session(session)


if __name__ == '__main__':
    main()
