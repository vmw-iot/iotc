import board
import busio

import adafruit_tcs34725
import adafruit_sht31d
import adafruit_bme280
import adafruit_bme680

i2c = busio.I2C(board.SCL, board.SDA)

# color sensor, i2c addr: 0x29
print('Test for tcs34725 (addr: 0x29)')
tcs34725 = adafruit_tcs34725.TCS34725(i2c)
print('Color: ({0}, {1}, {2}), Temperature: {3:.2f}K, Lux: {4:.2f}'.format(*tcs34725.color_rgb_bytes, tcs34725.color_temperature, tcs34725.lux))

# humidity & temp, i2c addr: 0x44
print('\nTest for sht31 (addr: 0x44)')
sht31 = adafruit_sht31d.SHT31D(i2c)
print('Temperature: {0:.2f}C, Humidity: {1:.2f}%'.format(sht31.temperature, sht31.relative_humidity))
print('Sensor Heater status = {}'.format(sht31.heater))

# pressure/hum/temp, i2c addr: 0x76
print('\nTest for bme280 (addr: 0x76)')
bme280 = adafruit_bme280.Adafruit_BME280_I2C(i2c, address=118)
bme280.sea_level_pressure = 1013.25
print("Temperature: {0:.2f}C, Pressure: {1:.2f} hPa, Altitude: {2:.2f} m".format(bme280.temperature, bme280.pressure, bme280.altitude))

# pressure/hum/temp/gas, i2c addr: 0x77
print('\nTest for bme680 (addr: 0x77)')
bme680 = adafruit_bme680.Adafruit_BME680_I2C(i2c, debug=False)
bme680.sea_level_pressure = 1013.25
print('Temperature: {0:.2f}C, Humidity: {1:.2f}%, Pressure: {2:.2f}hPa, Gas: {3:.2f}Ohm, Altitude: {4:.2f} m'.format(bme680.temperature, bme680.humidity, bme680.pressure, bme680.gas, bme680.altitude))
