#!/opt/myiot/venv/bin/python

import logging
import board
import busio
import adafruit_ccs811
import adafruit_sht31d
from pulse.settings import APP_ID, THING_CCS811_ID
from pulse.client import open_session, close_session, send_metric


def get_env():
    i2c = busio.I2C(board.SCL, board.SDA)
    sensor = adafruit_sht31d.SHT31D(i2c)
    temp = round(sensor.temperature, 2)
    hum = round(sensor.relative_humidity)
    return temp, hum


def main():
    i2c = busio.I2C(board.SCL, board.SDA)
    sensor = adafruit_ccs811.CCS811(i2c)

    temp, hum = get_env()
    sensor.set_environmental_data(hum, temp)
    logging.info(f'Set environmental_data, hum = {hum}, temp = {temp}')

    session = open_session(f'{APP_ID}.thing.ccs811')

    eco = sensor.eco2
    result = send_metric(session, THING_CCS811_ID, 'Equivalent Carbon Dioxide', eco)
    logging.info(f'Sent metric Equivalent Carbon Dioxide = {eco} with result = {result}')

    tvoc = sensor.tvoc
    result = send_metric(session, THING_CCS811_ID, 'Total Volatile Organic Compound', tvoc)
    logging.info(f'Sent metric Total Volatile Organic Compound = {tvoc} with result = {result}')

    close_session(session)


if __name__ == '__main__':
    main()
