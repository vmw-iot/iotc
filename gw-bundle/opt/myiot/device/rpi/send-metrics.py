#!/opt/myiot/venv/bin/python

import logging
import psutil
from array import array
from rpi import vcgencmd
from pulse.settings import APP_ID, RPI_GW0_ID
from pulse.client import open_session, close_session, send_metric


def get_soc_temp():
    """Returns the temperature of the SoC as measured by the on-board temperature sensor

    Here I'm using `vcgencmd measure_temp`

    :return: soc temperature as float
    """
    temp = vcgencmd(['measure_temp'])
    return float(temp[5:-2])


def get_arm_freq():
    """Returns the current frequency of the ARM

    :return: arm clock frequency in MHz as int
    """
    freq = vcgencmd(['measure_clock', 'arm'])
    return int(freq[14:]) // 1000000


def get_vc4_freq():
    """Returns the current frequency of the VC4 Scaler Cores

    :return: vc4 scaler cores clock frequency in MHz as int
    """
    freq = vcgencmd(['measure_clock', 'core'])
    return int(freq[13:]) // 1000000


def get_vc4_v():
    """Returns the current voltage used by the VC4 Core

    :return: vc4 core voltage in Volts as float
    """
    freq = vcgencmd(['measure_volts', 'core'])
    return float(freq[5:-1])


def get_sdramc_v():
    """Returns the current voltage used by the SDRAM-C

    :return: sdram-c voltage in Volts as float
    """
    freq = vcgencmd(['measure_volts', 'sdram_c'])
    return float(freq[5:-1])


def get_sdrami_v():
    """Returns the current voltage used by the SDRAM-I

    :return: sdram-i voltage in Volts as float
    """
    freq = vcgencmd(['measure_volts', 'sdram_i'])
    return float(freq[5:-1])


def get_sdramp_v():
    """Returns the current voltage used by the SDRAM-P

    :return: sdram-p voltage in Volts as float
    """
    freq = vcgencmd(['measure_volts', 'sdram_p'])
    return float(freq[5:-1])


def get_cpu_usage():
    """Returns average CPU Usage from last call First call will return average CPU usage from boot

    Calculations used:
        - ioWaitTime is included as part of total idle time. In most of cases, CPU time spent in iowait
          can be used if CPU intensive tasks are available
        - guestTime and guestNiceTime are not used. In kernel implementation, these
          values aren included in userTime and niceTime values

    :return: cpu average usage as float (0-100)
    """
    stats_file = '/tmp/cpu_stats'
    cpu = psutil.cpu_times()

    idle_time = cpu.idle + cpu.iowait
    busy_time = cpu.user + cpu.nice + cpu.system + cpu.irq + cpu.softirq + cpu.steal
    total_time = idle_time + busy_time

    last_stats = array('f', [0, 0, 0])  # cpu stats array: [total, busy, avg]

    try:  # read last stats from file
        f = open(stats_file, 'rb')
        last_stats.fromfile(f, 3)   # read three floats from file
        f.close()
        logging.info(f'Last CPU stats: total = {last_stats[0]}, busy = {last_stats[1]}, avg = {last_stats[2]}')
    except:
        logging.info(f'File {stats_file} does not exists')

    avg = 0
    if total_time > last_stats[0]:
        avg = 100.0 * (busy_time - last_stats[1]) / (total_time - last_stats[0])
        last_stats[0] = total_time
        last_stats[1] = busy_time
        last_stats[2] = avg
    else:
        avg = last_stats[3]

    with open(stats_file, 'wb') as f:  # write last stats to file
        last_stats.tofile(f)

    return round(avg, 2)


def get_mem_usage():
    """Get current memory usage

    :return: memory usage (0-100)
    """
    mem = psutil.virtual_memory()

    mem_used = mem.total - mem.free - mem.cached - mem.slab - mem.buffers
    if mem_used < 0:
        mem_used = mem.total - mem.free

    return round((100 * mem_used) / mem.total, 2)


def get_disk_usage():
    """Get current disk usage

    :return: disk usage (0-100)
    """
    disk = psutil.disk_usage('/')
    return disk.percent


def main():
    soc_temp = get_soc_temp()
    arm_freq = get_arm_freq()
    vc4_freq = get_vc4_freq()
    vc4_v = get_vc4_v()
    sdramc_v = get_sdramc_v()
    sdramp_v = get_sdramp_v()
    sdrami_v = get_sdrami_v()
    cpu_usage = get_cpu_usage()
    mem_usage = get_mem_usage()
    disk_usage = get_disk_usage()

    session = open_session(APP_ID)

    result = send_metric(session, RPI_GW0_ID, 'CPU-Usage', cpu_usage)
    logging.info(f'Sent metric CPU Usage = {cpu_usage} with result = {result}')

    result = send_metric(session, RPI_GW0_ID, 'Memory-Usage', mem_usage)
    logging.info(f'Sent metric Memory Usage = {mem_usage} with result = {result}')

    result = send_metric(session, RPI_GW0_ID, 'Disk-Usage', disk_usage)
    logging.info(f'Sent metric Disk Usage = {disk_usage} with result = {result}')

    result = send_metric(session, RPI_GW0_ID, 'SoC Temperature', soc_temp)
    logging.info(f'Sent metric SoC Temp = {soc_temp} with result = {result}')

    result = send_metric(session, RPI_GW0_ID, 'ARM Clock Frequency', arm_freq)
    logging.info(f'Sent metric ARM Clock Frequency = {arm_freq} with result = {result}')

    result = send_metric(session, RPI_GW0_ID, 'VC4 Scalar Cores Clock Frequency', vc4_freq)
    logging.info(f'Sent metric VC4 Scalar Cores Clock Frequency = {vc4_freq} with result = {result}')

    result = send_metric(session, RPI_GW0_ID, 'VC4 Core Voltage', vc4_v)
    logging.info(f'Sent metric VC4 Core Voltage = {vc4_v} with result = {result}')

    result = send_metric(session, RPI_GW0_ID, 'SDRAM-C Voltage', sdramc_v)
    logging.info(f'Sent metric SDRAM-C Voltage = {sdramc_v} with result = {result}')

    result = send_metric(session, RPI_GW0_ID, 'SDRAM-P Voltage', sdramp_v)
    logging.info(f'Sent metric SDRAM-P Voltage = {sdramp_v} with result = {result}')

    result = send_metric(session, RPI_GW0_ID, 'SDRAM-I Voltage', sdrami_v)
    logging.info(f'Sent metric SDRAM-I Voltage = {sdrami_v} with result = {result}')

    close_session(session)


if __name__ == '__main__':
    main()
