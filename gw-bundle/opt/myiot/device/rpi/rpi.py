import subprocess


def vcgencmd(args):
    """Wrapper around vcgencmd

    :param args: array with vcgencmd arguments
    :return: std output from vcgencmd
    """
    cmd = ['vcgencmd']
    for arg in args:
        cmd.append(arg)
    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    stdout, stderr = proc.communicate()
    return stdout.decode('utf-8').rstrip()
