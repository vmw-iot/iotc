import iotc


def main():
    gateway_id = '8a6bb65e-a26a-4ffe-92a7-4ee9c0762da5'
    payload = {"childTemplates": ["b6b5464d-b6f8-4604-b2a3-35187aae5c98"]}
    tid = iotc.update_device_template(gateway_id, payload)
    print('Updated Gateway with ID = {}'.format(tid))


if __name__ == '__main__':
    main()
