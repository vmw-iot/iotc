import os
import ssl
import json
import base64
import urllib.parse
import urllib.request

ssl._create_default_https_context = ssl._create_unverified_context
headers = {
    'Content-Type': 'application/json',
    'Accept': 'application/json;api-version=1.0',
    'Cache-Control': 'no-cache',
    'Connection': 'keep-alive',
    #    'Host'            : '{}:443'.format(config.PULSE_HOST),
    'accept-encoding': 'gzip, deflate'
}


def encode_creds(username, password):
    creds = '{}:{}'.format(username, password)
    creds = base64.b64encode(creds.encode('ascii'))
    return creds.decode('ascii')


def get(url, auth_header=None):
    hdr = dict(headers)
    if auth_header:
        hdr['Authorization'] = auth_header

    request = urllib.request.Request(url, headers=hdr)
    with urllib.request.urlopen(request) as response:
        data = response.read()
        return json.loads(data)


def post(url, payload, auth_header):
    # data = str(payload)
    data = json.dumps(payload)
    data = data.encode('utf-8')

    hdr = dict(headers)
    if auth_header:
        hdr['Authorization'] = auth_header
    hdr['Content-Length'] = len(data)

    request = urllib.request.Request(url, data=data, headers=hdr)
    with urllib.request.urlopen(request) as response:
        data = response.read()
        return json.loads(data)


def post_file(file, url, auth_header):
    """POST a file to the IoTC

    :param file: local file with full path included
    :param url: url for uploading
    :param auth_header: authorization header with device token
    :return: json with response which contains uploaded file identifier
    """
    hdr = dict(headers)
    hdr['Authorization'] = auth_header
    hdr['Content-Type'] = 'application/octet-stream'
    hdr['Content-Length'] = os.path.getsize(file)

    with open(file, 'rb') as f:
        request = urllib.request.Request(url, data=f.read(), headers=hdr)
        with urllib.request.urlopen(request) as response:
            data = response.read()
            return json.loads(data)


def put(url, payload, auth_header):
    data = str(payload)
    data = data.encode('utf-8')

    hdr = dict(headers)
    if auth_header:
        hdr['Authorization'] = auth_header
    hdr['Content-Length'] = len(data)

    request = urllib.request.Request(url, data=data, headers=hdr, method='PUT')
    with urllib.request.urlopen(request) as response:
        data = response.read()
        return json.loads(data)


def delete(url, auth_header):
    hdr = dict(headers)
    if auth_header:
        hdr['Authorization'] = auth_header

    request = urllib.request.Request(url, headers=hdr, method='DELETE')
    with urllib.request.urlopen(request) as response:
        data = response.read()
        return json.loads(data)
