#!/usr/bin/python3

import glob
import json
from pulse import iotc


def create_template(name, device_type, children=None):
    """Create device template

    :param name: name of the device template (sub-folder inside templates)
    :param device_type: type of the device: `gateway` or `thing`
    :param children: children IDs as array
    """
    template_folder = f'./templates/{device_type}s/{name}'
    template_file = f'{template_folder}/template.json'
    images = glob.glob(f'{template_folder}/image-?.png')

    # uploading device images and get their ids back
    image_ids = []
    for image in images:
        image_ids.append(iotc.upload_image(image))

    # load template json
    with open(template_file) as j:
        template = json.load(j)

    return iotc.create_device_template(template, image_ids, children)


def main():
    things = [
        # create_template('sht31', 'thing'),
        # create_template('bme680', 'thing'),
        # create_template('bh1745', 'thing'),
        # create_template('tcs34725', 'thing'),
        create_template('ccs811', 'thing'),
        # create_template('virtual-thp', 'thing')
    ]
    # create_template('rpi', 'gateway', things)

    print(things)


if __name__ == '__main__':
    main()
