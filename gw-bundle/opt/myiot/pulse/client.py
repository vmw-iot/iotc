# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------------------
# Python wrapper for Pulse IoT Center Agent SDK
#
# This file was written from the code of `PulseClient.py` and `iotcAgent.h` from the Agent.
# Module contains the APIs exported by the IoTC Agent as well as useful functions
# to work with Agent with high level requests.
#
# Alexey Malashin (amalashin@vmware.com)
# ------------------------------------------------------------------------------------------

import logging
from os.path import isfile
from time import time
from ctypes import cdll, Structure, Union, POINTER, CFUNCTYPE, byref, cast, \
    c_char, c_char_p, c_wchar_p, c_ubyte, c_int, c_int64, c_uint64, c_double, c_bool, c_size_t, c_void_p

logging.basicConfig(level=logging.INFO, format='%(asctime)s [%(name)-12.12s] [%(levelname)-5.5s]  %(message)s')
libsdk = cdll.LoadLibrary('/opt/vmware/iotc-agent/lib/libiotc-agent-sdk.so')

CLIENT_ID = 'ru.vmwlab.pulse-iot'   # default client identifier
CLIENT_TIMEOUT = 120000             # timeout for waiting response from agent (in milliseconds)

IOTC_APP_ID_SIZE = 65               # Max size of an application identifier
IOTC_NAME_MAX_SIZE = 256            # Max size for a name string
IOTC_UUID_SIZE = 37                 # UUID Size
IOTC_METRIC_NAME_SIZE = 64          # Max size of metric name
IOTC_METRIC_STRING_VALUE_SIZE = 32  # Max size of metric string data point
IOTC_VALUE_MAX_SIZE = 512           # Max size for a value string, used in device property name-value pairs
IOTC_PATH_MAX  = 4096               # Max file path size
IOTC_PAYLOAD_MAX_SIZE = 4096        # Max size for the payloads

# Supported response message types
(IOTC_INVALID_RESPONSE,
 IOTC_NOTIFICATION_RESPONSE,
 IOTC_ENROLL_RESPONSE,
 IOTC_UNENROLL_RESPONSE,
 IOTC_CAMPAIGN_STATE_CHANGE,
 IOTC_SCHEDULE_RESPONSE,
 IOTC_SET_PROGRESS,
 IOTC_SEND_METRIC,
 IOTC_UPLOAD_FILE,
 IOTC_GET_COMMANDS_FINISHED,
 IOTC_REGISTER_CB,
 IOTC_SEND_PROPERTIES,
 IOTC_GET_SYSTEM_PROPERTIES,
 IOTC_GET_CUSTOM_PROPERTIES,
 IOTC_GET_DEVICES,
 IOTC_NO_RESPONSE
 ) = map(c_int, range(16))

# Supported the metric unit types
(IOTC_METRIC_ERROR,
 IOTC_METRIC_STRING,
 IOTC_METRIC_INT64,
 IOTC_METRIC_DOUBLE,
 IOTC_METRIC_BOOLEAN,
 IOTC_METRIC_UNKNOWN
 ) = map(c_int, range(6))

# Status of the metric response sent from Agent-SDK
(IOTC_METRIC_SUCCESS,          # Metric successfully sent to Server
 IOTC_METRIC_STORED,           # Metric successfully stored at Agent
 IOTC_METRIC_NOT_ALLOWED,      # Metric is not in allowed list
 IOTC_METRIC_FAILED,           # Metric failed to be stored at agent or sent to server
 IOTC_METRIC_LIMIT_EXCEEDED,   # Metric data received is too big, retry remaining
 IOTC_METRIC_RETRY             # Server offline and metric store limit reached, retry later
 ) = map(c_int, range(6))

# Supported types of arguments in commands
(IOTC_CMD_ARG_VAL_INTEGER,
 IOTC_CMD_ARG_VAL_DOUBLE,
 IOTC_CMD_ARG_VAL_STRING,
 IOTC_CMD_ARG_VAL_UNKNOWN
 ) = map(c_int, range(4))


class IotcApplicationId(Structure):
    """Application identifier
    Could be any string with max length of (:const:`IOTC_APP_ID_SIZE` - 1)

    It is used to identify an application uniquely during exchange of data
    between client side application and server side applications
    It is recommended to use the reverse domain name notation
    e.g., com.vmware.iotc.agent.

    **Fields**
        - `id` : actual string of the identifier
    """
    _fields_ = [('id', c_char * IOTC_APP_ID_SIZE)]


class IotcDeviceId(Structure):
    """Device Identifier

    **Fields**
        - `id` : actual string of the identifier
    """
    _fields_ = [('id', c_char * IOTC_UUID_SIZE)]


class IotcDeviceDetails(Structure):
    """Device details

    **Fields**
        - `name` : name of the device
        - `deviceTemplate` : name of the device template
    """
    _fields_ = [
        ('name', c_char * IOTC_NAME_MAX_SIZE),
        ('deviceTemplate', c_char * IOTC_NAME_MAX_SIZE)
    ]


class IotcGetResponse(Structure):
    """GetResponse sent from the agent to the SDK

    **Fields**
        - `messageId` : message identifier
        - `type` : message type :class:`IotcGetResponseMsgType`
        - `response` : pointer to response
    """
    _fields_ = [('messageId', c_uint64), ('type', c_int), ('response', c_void_p)]


class IotcDevice(Structure):
    """Device Entity

    **Fields**
        - `deviceId` : device identifier :class:`IotcDeviceId`
        - `type` : device type :class:`IotcDeviceType`
    """
    _fields_ = [('deviceId', IotcDeviceId), ('type', c_int)]


class IotcDeviceSet(Structure):
    """Set of the devices

    **Fields**
        - `device` : pointer to devices array :class:`IotcDevice`
        - `used` : devices count
        - `size` : ??
    """
    _fields_ = [('device', POINTER(IotcDevice)), ('used', c_size_t), ('size', c_size_t)]


class IotcStringValue(Structure):
    """String type metric data point

    **Fields**
        - `ts` : timestamp
        - `value` : metric string value
    """
    _fields_ = [('ts', c_uint64), ('value', c_char * IOTC_METRIC_STRING_VALUE_SIZE)]


class IotcDoubleValue(Structure):
    """Float type metric data point

    **Fields**
        - `ts` : timestamp
        - `value` : metric double value
    """
    _fields_ = [('ts', c_uint64), ('value', c_double)]


class IotcInt64Value(Structure):
    """Integer type metric data point

    **Fields**
        - `ts` : timestamp
        - `value` : metric int64 value
    """
    _fields_ = [('ts', c_uint64), ('value', c_int64)]


class IotcBooleanValue(Structure):
    """Boolean type metric data point

    **Fields**
        - `ts` : timestamp
        - `value` : metric boolean value
    """
    _fields_ = [('ts', c_uint64), ('value', c_ubyte)]


class IotcMetricValueUnion(Union):
    """Metric data point to be sent to Agent

    **Fields**
        - `strings` : metric string values
        - `integers` : metric integer values
        - `doubles` : metric double values
        - `bools` : metric boolean values
    """
    _fields_ = [
        ('strings', IotcStringValue * 1),
        ('integers', IotcInt64Value * 1),
        ('doubles', IotcDoubleValue * 1),
        ('bools', IotcBooleanValue * 1)
    ]


class IotcMetric(Structure):
    """Metric data point to be sent to Agent

    **Fields**
        - `type` : metric type :class:`IotcMetricType`
        - `deviceId` : device identifier :class:`IotcDeviceId`
        - `name` : name of the metric
        - `metricValues` : metric values union :class:`IotcMetricValueUnion`
    """
    _anonymous_ = ('metricValues',)
    _fields_ = [
        ('type', c_int),
        ('deviceId', IotcDeviceId),
        ('name', c_char * IOTC_METRIC_NAME_SIZE),
        ('metricValues', IotcMetricValueUnion)
    ]


class IotcMetricResponse(Structure):
    """Metric response

    **Fields**
        - `status` : metric status :class:`IotcMetricResponseStatus`
        - `metric` : metric info whose response received from agent :class:`IotcMetric`
    """
    _fields_ = [('status', c_int), ('metric', IotcMetric * 0)]


class IotcKeyValue(Structure):
    """Represents a key value pair

    **Fields**
        - `key` : key with max size of `IOTC_NAME_MAX_SIZE`
        - `value` : value with max size of `IOTC_VALUE_MAX_SIZE`
    """
    _fields_ = [('key', c_char * IOTC_NAME_MAX_SIZE), ('value', c_char * IOTC_VALUE_MAX_SIZE)]


class IotcKeyValueSet(Structure):
    """Contains an array of device properties

    **Fields**
        - `keyValue` : pointer to array with properties :class:`IotcKeyValue`
        - `used` : represents the number of current keyValue set
        - `size` : represents the capacity of the keyValue set
    """
    _fields_ = [('keyValue', POINTER(IotcKeyValue)), ('used', c_size_t), ('size', c_size_t)]


class IotcCommandArgValueUnion(Union):
    """Values union of the command

    **Fields**
        - `intValues` : command integer values
        - `doubleValues` : command double values
        - `strValues` : command string values
    """
    _fields_ = [
        ('intValues', POINTER(c_int64)),
        ('doubleValues', POINTER(c_double)),
        ('strValues', POINTER(c_char_p)),
    ]


class IotcCommandArg(Structure):
    """Command argument

    **Fields**
        - `type` : type of the value :class:`IotcCommandArgValueType`
        - `name` : name of the argument with max size of `IOTC_NAME_MAX_SIZE`
        - `numValues` : number of items in the values array
        - `argValues` : values array of the argument :class:`IotcCommandArgValueUnion`
    """
    _fields_ = [
        ('type', c_int),
        ('name', c_char * IOTC_NAME_MAX_SIZE),
        ('numValues', c_size_t),
        ('argValues', IotcCommandArgValueUnion)
    ]


class IotcCommand(Structure):
    """Command holds details about a command msg received from the server

    **Fields**
        - `name` : friendly name of the command with max size of `IOTC_NAME_MAX_SIZE`
        - `id` : command identifier generated by the server with max size of `IOTC_UUID_SIZE`
        - `deviceId` : device identifier for the device which this command is targetted (optional) :class:`IotcDeviceId`
        - `execPath` : absolute path to the executable (optional) with max size of `IOTC_PATH_MAX`
        - `numArgs` : number of arguments for the command
        - `args` : list of arguments for the command, pointer to :class:`IotcCommandArg`
    """
    _fields_ = [
        ('name', c_char * IOTC_NAME_MAX_SIZE),
        ('id', c_char * IOTC_UUID_SIZE),
        ('deviceId', IotcDeviceId),
        ('execPath', c_char * IOTC_PATH_MAX),
        ('numArgs', c_size_t),
        ('args', POINTER(IotcCommandArg))
    ]


class IotcCommandResponse(Structure):
    """Command response to hold response received for a command

    **Fields**
        - `message` : usually error message that has to be sent to the server with max size of `IOTC_PAYLOAD_MAX_SIZE`
    """
    _fields_ = [('message', c_char * IOTC_PAYLOAD_MAX_SIZE)]


def get_timestamp_ms():
    """Return the current time in number of milliseconds since UNIX epoch time.
    :return: an `int` that represents the current time
    """
    return int(round(time() * 1000))


def open_session(app_id):
    """Initializes the communication channel with the IoTC Agent

    :param app_id: the application identifier of the invoking client
    :return: pointer to the :class:`IotcSession` object on success or NULL on failure
    """
    iotc_app_id = IotcApplicationId()
    if app_id is None:
        iotc_app_id.id = CLIENT_ID.encode('utf-8')
    else:
        iotc_app_id.id = app_id.encode('utf-8')

    libsdk.Iotc_Init.restype = c_void_p
    libsdk.Iotc_Init.argtypes = [POINTER(IotcApplicationId)]

    session = c_void_p()
    session = libsdk.Iotc_Init(byref(iotc_app_id))
    return session


def close_session(iotc_session):
    """Closes the communication channel with the IoTC Agent

    :param iotc_session: pointer to session object returned as part of the :func:`open_session` call
    """
    libsdk.Iotc_Close.argtypes = [c_void_p]
    libsdk.Iotc_Close(iotc_session)


@CFUNCTYPE(c_int, POINTER(IotcCommand), POINTER(IotcCommandResponse), POINTER(c_void_p))
def commands_cb(cmd, resp, data):
    """Callback function to receive a command from the Agent

    :param cmd: points to the client command
    :param resp: command response to be updated by the client
    :param data: pointer to context data supplied during client callback registration
    :returns: 0 to indicate successful processing of client command and -1 to indicate failure
    """
    iotc_cmd = cmd.contents
    logging.info(f'Command Callback: command id = {iotc_cmd.id}, name = {iotc_cmd.name}, device id = {iotc_cmd.deviceId.id}')
    logging.info(f'Command Callback: exec path = {iotc_cmd.execPath}, arguments count = {iotc_cmd.numArgs}')

    args = {}
    for aidx in range(iotc_cmd.numArgs):
        name = iotc_cmd.args[aidx].name.decode()
        args[name] = []
        # print(f'name = {iotc_cmd.args[aidx].name}, type = {iotc_cmd.args[aidx].type}, values num = {iotc_cmd.args[aidx].numValues}')
        for vidx in range(iotc_cmd.args[aidx].numValues):
            t, v = 'N/A', 'N/A'
            if iotc_cmd.args[aidx].type == IOTC_CMD_ARG_VAL_INTEGER.value:
                t, v = 'int', iotc_cmd.args[aidx].argValues.intValues[vidx]
            elif iotc_cmd.args[aidx].type == IOTC_CMD_ARG_VAL_DOUBLE.value:
                t, v = 'float', iotc_cmd.args[aidx].argValues.doubleValues[vidx]
            elif iotc_cmd.args[aidx].type == IOTC_CMD_ARG_VAL_STRING.value:
                t, v = 'str', iotc_cmd.args[aidx].argValues.strValues[vidx].decode()
            args[name].append(v)
            # print(f'\tvalue "{vidx}" with type "{t}" = "{v}"')

    if 'Command' in args.keys() and isfile(args['Command'][0]):
        runfile(args["Command"][0], args=args['Arguments'])
        logging.info(f'Run {args["Command"][0]} with {args["Arguments"]}')

    return 0


def register_cb(iotc_session):
    """Register a command callback to receive commands from the Agent if any

    :param iotc_session: IoTC session :class:`IotcSession`
    :returns: 0 on successful registration, -1 on errors
    """
    status = libsdk.Iotc_RegisterCommandCallback(iotc_session, commands_cb, None)
    logging.info(f'Callback registration status = {"registered" if not status else "failure"}')

    return status


def get_devices(iotc_session, device_id):
    """(**DEPRECATED**) Retrieves all the connected devices for a device with its id & type

    :param iotc_session: IoTC session :class:`IotcSession`
    :param device_id: device identifier string
    :return: dict with device ids and types
    """

    # Send request for Device ID
    iotc_device_id = IotcDeviceId()
    if device_id is None:
        iotc_device_id.id = b''
    else:
        iotc_device_id.id = device_id.encode('utf-8')

    libsdk.Iotc_GetDevices.restype = c_int
    libsdk.Iotc_GetDevices.argtypes = [c_void_p, POINTER(IotcDeviceId)]

    status = libsdk.Iotc_GetDevices(iotc_session, byref(iotc_device_id))
    if status == -1:
        logging.error('Failed sending get devices request')

    # Get the response
    iotc_get_response = IotcGetResponse()
    libsdk.Iotc_GetResponseByType.restype = c_int
    libsdk.Iotc_GetResponseByType.argtypes = [c_void_p, c_int, c_int, POINTER(IotcGetResponse)]
    status = libsdk.Iotc_GetResponseByType(iotc_session, IOTC_GET_DEVICES, CLIENT_TIMEOUT, byref(iotc_get_response))
    if status == -1 or iotc_get_response.type == -1:
        logging.error('Failed with devices request response or status wrong')

    # Get Device Set
    iotc_device_set = cast(iotc_get_response.response, POINTER(IotcDeviceSet))
    iotc_device_set = iotc_device_set.contents
    logging.info(f'Found {iotc_device_set.used} devices')

    # Construct the devices dictionary from the Devices Set
    devices = []
    devices_p = cast(iotc_device_set.device, POINTER(IotcDevice))
    for i in range(iotc_device_set.used):
        logging.debug(f'Device ID={devices_p[i].deviceId.id} with type={devices_p[i].type}')
        devices.append({'id': devices_p[i].deviceId.id.decode('utf-8'), 'type': devices_p[i].type})

    libsdk.Iotc_FreeGetResponse(byref(iotc_get_response))

    return devices


def get_commands(iotc_session):
    """Handler function for handling get command operation

    :param iotc_session: IoTC session :class:`IotcSession`
    :return: pending device commands if any
    """
    # send GetCommands request to Agent
    libsdk.Iotc_GetCommands.restype = c_int
    libsdk.Iotc_GetCommands.argtypes = [c_void_p]

    status = libsdk.Iotc_GetCommands(iotc_session)
    if status == -1:
        logging.error('Failed sending get commands request')
    else:
        logging.info('Commands request sent successfully')

    # Get the response from Agent
    iotc_get_response = IotcGetResponse()
    libsdk.Iotc_GetResponseByType.restype = c_int
    libsdk.Iotc_GetResponseByType.argtypes = [c_void_p, c_int, c_int, POINTER(IotcGetResponse)]
    status = libsdk.Iotc_GetResponseByType(iotc_session, IOTC_GET_COMMANDS_FINISHED, CLIENT_TIMEOUT, byref(iotc_get_response))
    if status == -1 or iotc_get_response.type == -1:
        logging.error('Failed with commands request response or status wrong')
    else:
        logging.info('Got the commands request')

    libsdk.Iotc_FreeGetResponse(byref(iotc_get_response))


def send_metric(iotc_session, device_id, name, value):
    """Send single metric data point to the Agent to be sent to the server

    Metric type will be set from the `value` parameter type

    :param iotc_session: IoTC session :class:`IotcSession`
    :param device_id: device identifier string
    :param name: name of the metric
    :param value: value of the metric
    :return: status of the send metric operation
    """

    # Prepare metric information
    iotc_metric = IotcMetric()
    iotc_metric.deviceId = IotcDeviceId()
    iotc_metric.deviceId.id = device_id.encode('utf-8')
    iotc_metric.name = name.encode('utf-8')
    timestamp_c = c_uint64(get_timestamp_ms())

    # Check and set metric value and type
    metric_type = type(value)
    if metric_type == int:
        iotc_metric.type = IOTC_METRIC_INT64
        iotc_metric.integers = (IotcInt64Value * 1)()
        iotc_metric.integers[0].ts = timestamp_c
        iotc_metric.integers[0].value = c_int64(value)
    elif metric_type == float:
        iotc_metric.type = IOTC_METRIC_DOUBLE
        iotc_metric.doubles = (IotcDoubleValue * 1)()
        iotc_metric.doubles[0].ts = timestamp_c
        iotc_metric.doubles[0].value = c_double(value)
    elif metric_type == bool:
        iotc_metric.type = IOTC_METRIC_BOOLEAN
        iotc_metric.bools = (IotcBooleanValue * 1)()
        iotc_metric.bools[0].ts = timestamp_c
        iotc_metric.bools[0].value = c_bool(value)
    elif metric_type == str:
        iotc_metric.type = IOTC_METRIC_STRING
        iotc_metric.strings = (IotcStringValue * 1)()
        iotc_metric.strings[0].ts = timestamp_c
        iotc_metric.strings[0].value = value

    # Send metric to agent
    libsdk.Iotc_SendMetric.restype = c_int
    libsdk.Iotc_SendMetric.argtypes = [c_void_p, POINTER(IotcMetric)]

    status = libsdk.Iotc_SendMetric(iotc_session, byref(iotc_metric))
    if status == -1:
        logging.error('Failed send metric')

    # Get the response
    iotc_get_response = IotcGetResponse()
    libsdk.Iotc_GetResponseByType.restype = c_int
    libsdk.Iotc_GetResponseByType.argtypes = [c_void_p, c_int, c_int, POINTER(IotcGetResponse)]
    status = libsdk.Iotc_GetResponseByType(iotc_session, IOTC_SEND_METRIC, CLIENT_TIMEOUT, byref(iotc_get_response))
    if status == -1 or iotc_get_response.type == -1:
        logging.error('Failed with send metric response or status wrong')

    # Get the metric response
    iotc_metric_response = cast(iotc_get_response.response, POINTER(IotcMetricResponse))
    iotc_metric_response = iotc_metric_response.contents
    logging.debug(f'Metric response status = {iotc_metric_response.status}')

    # Check the response
    status = iotc_metric_response.status
    if status == IOTC_METRIC_SUCCESS.value:
        logging.info('Metric successfully sent to Server')
    elif status == IOTC_METRIC_STORED.value:
        logging.info('Metric successfully stored at Agent')
    elif status == IOTC_METRIC_NOT_ALLOWED.value:
        logging.warning('Metric is not in allowed list')
    elif status == IOTC_METRIC_FAILED.value:
        logging.error('Metric failed to be stored at agent or sent to server')
    elif status == IOTC_METRIC_LIMIT_EXCEEDED.value:
        logging.warning('Metric data received is too big, retry remaining')
    elif status == IOTC_METRIC_RETRY.value:
        logging.info('Server offline and metric store limit reached, retry later')
    else:
        logging.error('Unknown status')

    libsdk.Iotc_FreeGetResponse(byref(iotc_get_response))

    return status


def send_properties(iotc_session, device_id, properties):
    """Send property set to the server

    :param iotc_session: IoTC session :class:`IotcSession`
    :param device_id: device identifier string
    :param properties: properties dictionary
    :return: 0 on success and -1 on error
    """
    iotc_device_id = IotcDeviceId()
    iotc_device_id.id = device_id.encode('utf-8')

    # Allocate property set
    iotc_kvset = IotcKeyValueSet()
    libsdk.Iotc_AllocateProperties.restype = c_int
    libsdk.Iotc_AllocateProperties.argtypes = [POINTER(IotcKeyValueSet), c_size_t]
    status = libsdk.Iotc_AllocateProperties(byref(iotc_kvset), len(properties))
    logging.debug(f'Property set allocation status {status}')

    # Fill property set with properties
    libsdk.Iotc_InsertProperties.restype = c_int
    libsdk.Iotc_InsertProperties.argtypes = [POINTER(IotcKeyValueSet), POINTER(IotcKeyValue)]
    for k, v in properties.items():
        iotc_property = IotcKeyValue()
        iotc_property.key = k.encode('utf-8')
        iotc_property.value = v.encode('utf-8')
        status = libsdk.Iotc_InsertProperties(byref(iotc_kvset), byref(iotc_property))
        logging.debug(f'Property {k} with value {v} inserted with status {status}')

    # Send properties to agent
    libsdk.Iotc_SendPropertySet.restype = c_int
    libsdk.Iotc_SendPropertySet.argtypes = [c_void_p, POINTER(IotcKeyValueSet), POINTER(IotcDeviceId)]
    status = libsdk.Iotc_SendPropertySet(iotc_session, byref(iotc_kvset), byref(iotc_device_id))
    logging.debug(f'Send property set status {status}')

    # Free allocated memory
    libsdk.Iotc_DeleteProperties.argtypes = [POINTER(IotcKeyValueSet)]
    libsdk.Iotc_DeleteProperties(byref(iotc_kvset))

    if status == -1:
        logging.error('Failed send properties')

    # Get the response
    iotc_get_response = IotcGetResponse()
    libsdk.Iotc_GetResponseByType.restype = c_int
    libsdk.Iotc_GetResponseByType.argtypes = [c_void_p, c_int, c_int, POINTER(IotcGetResponse)]
    status = libsdk.Iotc_GetResponseByType(iotc_session, IOTC_SEND_PROPERTIES, CLIENT_TIMEOUT, byref(iotc_get_response))
    if status == -1 or iotc_get_response.type == -1:
        logging.error('Failed with send properties response or status wrong')

    libsdk.Iotc_FreeGetResponse(byref(iotc_get_response))

    return status
