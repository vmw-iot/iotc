#!/opt/myiot/venv/bin/python

import logging
from bh1745 import BH1745
from pulse.settings import APP_ID, THING_BH1745_ID
from pulse.client import open_session, close_session, send_properties


def main():
    sensor = BH1745()
    sensor.setup()

    session = open_session(APP_ID)

    properties = {
        'i2c-address': hex(sensor._i2c_addr),
    }

    result = send_properties(session, THING_BH1745_ID, properties)
    logging.info(f'Sent property with result = {result}')

    close_session(session)


if __name__ == '__main__':
    main()
