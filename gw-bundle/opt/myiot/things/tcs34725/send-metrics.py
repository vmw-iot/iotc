#!/opt/myiot/venv/bin/python

import logging
import board
import busio
import adafruit_tcs34725
from pulse.settings import APP_ID, THING_TCS34725_ID
from pulse.client import open_session, close_session, send_metric


def main():
    i2c = busio.I2C(board.SCL, board.SDA)
    sensor = adafruit_tcs34725.TCS34725(i2c)

    session = open_session(APP_ID)

    rgb = sensor.color_rgb_bytes
    lux = round(sensor.lux, 2)
    temp = round(sensor.color_temperature)

    result = send_metric(session, THING_TCS34725_ID, 'Red Color', rgb[0])
    logging.info(f'Sent metric Red = {rgb[0]} with result = {result}')

    result = send_metric(session, THING_TCS34725_ID, 'Green Color', rgb[1])
    logging.info(f'Sent metric Green = {rgb[1]} with result = {result}')

    result = send_metric(session, THING_TCS34725_ID, 'Blue Color', rgb[2])
    logging.info(f'Sent metric Blue = {rgb[2]} with result = {result}')

    result = send_metric(session, THING_TCS34725_ID, 'Illuminance', lux)
    logging.info(f'Sent metric Illuminance = {lux} with result = {result}')

    result = send_metric(session, THING_TCS34725_ID, 'Color Temperature', temp)
    logging.info(f'Sent metric Color Temperature = {temp} with result = {result}')

    close_session(session)


if __name__ == '__main__':
    main()
