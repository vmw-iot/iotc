#!/opt/myiot/venv/bin/python

import logging
import random
from pulse.settings import APP_ID, GATEWAY_VIRT_ID
from pulse.client import open_session, close_session, send_metric


def main():
    session = open_session(APP_ID)
    result = send_metric(session, GATEWAY_VIRT_ID, 'Test-Metric', random.uniform(0, 100))
    logging.info(f'Sent metric with result = {result}')
    close_session(session)


if __name__ == '__main__':
    main()
