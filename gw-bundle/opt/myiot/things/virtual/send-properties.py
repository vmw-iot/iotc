#!/opt/myiot/venv/bin/python

import logging
import random
import string
from pulse.settings import APP_ID, THING_VIRTUAL_ID
from pulse.client import open_session, close_session, send_properties


def main():
    session = open_session(APP_ID)
    result = send_properties(session, THING_VIRTUAL_ID, {'uno': ''.join([random.choice(string.ascii_letters + string.digits) for n in range(10)])})
    logging.info(f'Sent property with result = {result}')
    close_session(session)


if __name__ == '__main__':
    main()
