#!/opt/myiot/venv/bin/python

from pulse.settings import APP_ID
from pulse.client import open_session, register_cb, get_commands, close_session


def main():
    iotc_session = open_session(f'{APP_ID}.gateway.rpi')
    register_cb(iotc_session)
    get_commands(iotc_session)
    close_session(iotc_session)


if __name__ == '__main__':
    main()
