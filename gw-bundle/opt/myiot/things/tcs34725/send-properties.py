#!/opt/myiot/venv/bin/python

import logging
import board
import busio
import adafruit_tcs34725
from pulse.settings import APP_ID, THING_TCS34725_ID
from pulse.client import open_session, close_session, send_properties


def main():
    i2c = busio.I2C(board.SCL, board.SDA)
    sensor = adafruit_tcs34725.TCS34725(i2c)

    session = open_session(APP_ID)

    properties = {
        'i2c-address': hex(sensor._device.device_address),
        'integration-time': str(sensor.integration_time),
        'gain': str(sensor.gain),
        'glass-attenuation': str(sensor.glass_attenuation)
    }

    result = send_properties(session, THING_TCS34725_ID, properties)
    logging.info(f'Sent properties with result = {result}')

    close_session(session)


if __name__ == '__main__':
    main()
