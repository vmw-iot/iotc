#!/opt/myiot/venv/bin/python

import sys
import logging
from time import sleep
from board import SCL, SDA
from busio import I2C
from adafruit_sht31d import SHT31D

logging.basicConfig(level=logging.INFO, format='%(asctime)s [%(name)-12.12s] [%(levelname)-5.5s]  %(message)s')


def prepare_bus():
    i2c = I2C(SCL, SDA)
    sensor = SHT31D(i2c)
    return sensor


def evaporate():
    sensor = prepare_bus()
    sensor.heater = True
    logging.info('Enabled heater for 5 secs')
    sleep(5)
    sensor.heater = False
    logging.info('Heater disabled')


def reset():
    sensor = prepare_bus()
    sensor._reset()
    logging.info('Sensor reset successfully')


def main():
    args = ['evaporate', 'reset']
    if len(sys.argv) != 2 or sys.argv[1] not in args:
        print(f'Usage: {sys.argv[0]} <reset|evaporate>')
        print('reset - Soft-reset sensor')
        print('evaporate - Enable heater for 10 secs to evaporate the sensor')
    elif sys.argv[1] == 'evaporate':
        logging.info('Evaporating sensor')
        evaporate()
    elif sys.argv[1] == 'reset':
        logging.info('Resetting sensor')
        reset()


if __name__ == '__main__':
    main()

