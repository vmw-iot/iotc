from base64 import b64encode
from pulse import rest

PULSE_HOST = 'pulse.vmwlab.ru'
PULSE_USER = 'amalashin@pulse.local'
PULSE_PWD = '.'
TEMPLATES_DIR = './templates'
ENROLL_FILE = '/opt/vmware/iotc-agent/data/data/enrollment.data'

MAX_UPLOAD_SIZE = 15 * 1024 * 1024  # IoTC limit for uploading


def get_access_token():
    auth = 'Basic {}'.format(rest.encode_creds(PULSE_USER, PULSE_PWD))
    token = rest.get('https://{}/api/tokens'.format(PULSE_HOST), auth_header=auth)
    return token['accessToken']


def get_device_enrollment():
    with open(ENROLL_FILE, 'rb') as f:
        bstr = f.read()
        header_offset = 4  # header offset
        device_id_len = 32 + 4  # 32 hex digits plus 4 dashes
        device_id = bstr[header_offset:header_offset + device_id_len].decode()
        device_token = bstr[header_offset + device_id_len:].decode()

    return device_id, device_token


def get_auth_header():
    return 'Bearer {}'.format(get_access_token())


def get_device_auth_header(token):
    return f'Bearer {token}'


def get_api_ver():
    api_ver = rest.get('https://{}/api/versions'.format(PULSE_HOST))
    return api_ver['currentApiVersion']


def get_device_templates():
    print('Get Device Templates')
    return rest.get('https://{}/api/device-templates'.format(PULSE_HOST), auth_header=get_auth_header())


def create_device_template(template, images_ids=None, children_ids=None):
    print(f'Creating a device template with name {template["name"]}')

    # add uploaded image ids to template json
    if images_ids is not None:
        template['imageDetails'] = [
            {'id': img_id, 'imageUrl': f'/api/devices/images/{img_id}', 'sourceType': 'base64'}
            for img_id in images_ids
        ]

    if children_ids is not None:
        template['childTemplates'] = children_ids

    data = rest.post(f'https://{PULSE_HOST}/api/device-templates', template, auth_header=get_auth_header())
    print(f'Done, Device Template ID = {data["id"]}')
    return data["id"]


def update_device_template(template_id, payload):
    print('Updating Device Template with ID = {}'.format(template_id))
    data = rest.put('https://{}/api/device-templates/{}'.format(PULSE_HOST, template_id), payload, auth_header=get_auth_header())
    print('Done, Device Template ID = {}'.format(data['id']))
    return data['id']


def create_device_credential(device_id):
    print('Creating Device Credential for ID = {}'.format(device_id))
    data = rest.post('https://{}/api/device-credentials/{}'.format(PULSE_HOST, device_id), {}, auth_header=get_auth_header())
    print('Done, New Credential = {}'.format(data['credentials']))
    return data['credentials']


def upload_file(src, filename, device_id, device_token):
    """Upload a file to the IoT Center

    :param src: full path to source file
    :param filename: file name how it will be presented in IoTC
    :param device_id: device identifier to associate a file with
    :param device_token: device token for authorization
    """
    print(f'Uploading file {src} for device {device_id}')
    url = f'https://{PULSE_HOST}/api/devices/files?filename={filename}&deviceId={device_id}'
    data = rest.post_file(src, url, auth_header=get_device_auth_header(device_token))
    print(f'Done, file {filename} uploaded with id = {data["id"]}')


def list_files():
    print('List files')
    return rest.get(f'https://{PULSE_HOST}/api/devices/files', auth_header=get_auth_header())


def upload_image(img_file):
    """Upload an image to IoTC

    :param img_file: image file path
    :return: uploaded image id
    """
    print('Uploading image')

    with open(img_file, 'rb') as i:
        img_base64 = b64encode(i.read()).decode()

    img_data = {'data': f'data:image/png;base64,{img_base64}'}
    data = rest.post(f'https://{PULSE_HOST}/api/devices/images', img_data, auth_header=get_auth_header())
    print(f'Done, image uploaded with id = {data["id"]}')

    return data['id']


def delete_image(image_id):
    print(f'Deleting image with id = {image_id}')
    data = rest.delete(f'https://{PULSE_HOST}/api/devices/images?id={image_id}', auth_header=get_auth_header())
