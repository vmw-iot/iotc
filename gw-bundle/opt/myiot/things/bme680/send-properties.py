#!/opt/myiot/venv/bin/python

import logging
import board
import busio
import adafruit_bme680
from pulse.settings import APP_ID, THING_BME680_ID
from pulse.client import open_session, close_session, send_properties


def main():
    i2c = busio.I2C(board.SCL, board.SDA)
    sensor = adafruit_bme680.Adafruit_BME680_I2C(i2c)

    session = open_session(APP_ID)

    properties = {
        'i2c-address': hex(sensor._i2c.device_address)
    }

    result = send_properties(session, THING_BME680_ID, properties)
    logging.info(f'Sent properties with result = {result}')

    close_session(session)


if __name__ == '__main__':
    main()
