#!/opt/myiot/venv/bin/python

import logging
import platform
from pulse.settings import APP_ID, GATEWAY_VIRT_ID
from pulse.client import open_session, close_session, send_properties


def main():
    session = open_session(APP_ID)
    result = send_properties(session, GATEWAY_VIRT_ID, {'kernel-version': platform.platform()})
    logging.info(f'Sent property with result = {result}')
    close_session(session)


if __name__ == '__main__':
    main()
