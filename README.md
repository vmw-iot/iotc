# Pulse IoT Center Preparation

1. Create device templates with `./prepare.py`. \
   Check [Device Management APIs](https://docs.vmware.com/en/VMware-Pulse-IoT-Center/2.0.1/pulse-api/GUID-64E0F46A-ABD0-47E3-8B53-AF46FB765047.html) -> **Create a Device Template** section for the reference.

2. Register a gateway in the IoTC

3. Create device token **Actions** -> **Create Device Credentials** in the IoTC and save the Credential somewhere

4. Create connected devices for the registered gateway and save their IDs

# Gateway preparation for the Pulse IoT Center

1. Login to Gateway to download and install Pulse Agent and LIOTA (*GRPC errors are common and expected at this stage*):  
   ```bash
   cd ~
   curl -ko pulseagent.tar.gz https://pulse.vmwlab.ru/api/iotc-agent/iotc-agent-arm-2.0.1-14825020.tar.gz
   curl -ko liota.tar.gz https://pulse.vmwlab.ru/api/liota/liota-2.0.1-14813117.tar.gz
   curl -ko bundle.tar.gz https://gitlab.com/vmw-iot/iotc/-/archive/master/iotc-master.tar.gz?path=gw-bundle
   tar -xvzf pulseagent.tar.gz
   tar -xvzf liota.tar.gz 
   tar -xvzf bundle.tar.gz   
   sudo iotc-agent/install.sh disable-defclient
   ```
    Check status: `journalctl -u iotc-agent -f`
    
2. Import CA certificates (root and intermediate) to the Gateway trusted CA's.
    Example for Debian:
    ```bash
    sudo mkdir /usr/local/share/ca-certificates/vmwlab
    sudo cp iotc-master-gw-bundle/gw-bundle/certificates/*.crt /usr/local/share/ca-certificates/vmwlab
    sudo chmod 755 /usr/local/share/ca-certificates/vmwlab
    sudo chmod 644 /usr/local/share/ca-certificates/vmwlab/*.crt
    sudo update-ca-certificates 
    ```
    or use this for Ubuntu: `sudo dpkg-reconfigure ca-certificates`

3. On-board the Gateway ([doc](https://docs.vmware.com/en/VMware-Pulse-IoT-Center/2.0.1/iotc-user-guide/GUID-B9E01259-68C6-4D91-9074-ECD41F6BA22C.html))
   and enroll connected things.
   SSH to device and issue the commands:
   ```bash
   sudo /opt/vmware/iotc-agent/bin/iotc-agent-cli enroll --auth-type=REGISTERED --token=<AUTH TOKEN>
   sudo /opt/vmware/iotc-agent/bin/iotc-agent-cli enroll-device --device-id=<THING1 ID>
   sudo /opt/vmware/iotc-agent/bin/iotc-agent-cli enroll-device --device-id=<THING2 ID>
   ...
   ```
   *Note*:
     - **AUTH TOKEN** - Credential saved from IoTC
     - **THING ID** - thing identifier from IoTC
   
   Do this for all the connected devices.

4. Update system software, install the dependencies and make changes just to be sure that everything will work:
   ```bash
   sudo apt update
   sudo apt upgrade
   sudo apt install python3 python3-dev python3-distutils python3-venv python-smbus i2c-tools
   curl https://bootstrap.pypa.io/get-pip.py -ko get-pip.py
   sudo python3 get-pip.py
   sudo usermod -a -G i2c,spi,gpio,video iotc
   sudo reboot
   ```
   *Note*: check `/etc/udev/rules.d/99-com.rules` for right groups.

5. Create and activate Python Virtual Environment
   ```bash
   sudo mkdir -p /opt/myiot/myiot.egg-info
   cd /opt/myiot
   sudo cp -r ~/iotc-master-gw-bundle/gw-bundle/opt/myiot/* /opt/myiot/
   sudo python3 -m venv venv --system-site-packages
   sudo chown -R iotc:iotc ./*
   sudo su - iotc
   source /opt/myiot/venv/bin/activate
   pip install -r requirements.txt
   ```

6. Install *myiot* project in the editable state (under *iotc* user)
   ```bash
   cd /opt/myiot
   pip install -e .
   ```

7. Set the **.py* scripts as executables (under *iotc* user)
   ```bash
   chmod +x /opt/myiot/device/virtual/*.py
   chmod +x /opt/myiot/device/rpi/*.py
   chmod +x /opt/myiot/things/virtual/*.py
   chmod +x /opt/myiot/things/sht3x/*.py
   chmod +x /opt/myiot/things/bh1745/*.py
   chmod +x /opt/myiot/things/bme680/*.py
   chmod +x /opt/myiot/things/tcs34725/*.py
   chmod +x /opt/myiot/things/vthp/*.py
   ```

8. Change settings for package at `/opt/myiot/pulse/settings.py`

9. Setup crontab to regularly send metrics/properties from device to the IoTC (under *iotc* user)
   ```bash
   crontab -e
   ```
   Example:
    - send **device** metrics every minute
    - send **device** properties once a day at 0:0
    - send **thing** metrics every five minutes
    - send **thing** properties once a week at sunday

   ```text
   * * * * *   /opt/myiot/device/rpi/send-metrics.py
   0 0 * * *   /opt/myiot/device/rpi/send-properties.py

   */5 * * * * /opt/myiot/things/sht3x/send-metrics.py
   0 0 * * 0   /opt/myiot/things/sht3x/send-properties.py

   */5 * * * * /opt/myiot/things/bme680/send-metrics.py
   0 2 * * 0   /opt/myiot/things/bme680/send-properties.py

   */5 * * * * /opt/myiot/things/bh1745/send-metrics.py
   0 4 * * 0   /opt/myiot/things/bh1745/send-properties.py
   
   */5 * * * * /opt/myiot/things/tcs34725/send-metrics.py
   0 4 * * 0   /opt/myiot/things/tcs34725/send-properties.py
   
   */3 * * * * /opt/myiot/things/vthp/send-metrics.py
   ```

10. Check the logs with `journalctl -u iotc-agent -f`

# Useful links

- [Pulse IoT Center Admin Guide](https://docs.vmware.com/en/VMware-Pulse-IoT-Center/2.0.1/administration.guide/GUID-30978216-64FD-49AB-BFC7-E36FEBD8E874.html) [[PDF](https://docs.vmware.com/en/VMware-Pulse-IoT-Center/2.0.1/administration.guide.pdf)]
- [Pulse IoT Center User Guide](https://docs.vmware.com/en/VMware-Pulse-IoT-Center/2.0.1/iotc-user-guide/GUID-B49C2C8C-1A88-403A-9743-07CA51E5B976.html) [[PDF](https://docs.vmware.com/en/VMware-Pulse-IoT-Center/2.0.1/iotc-user-guide.pdf)]
- [Pulse IoT Center API Guide](https://docs.vmware.com/en/VMware-Pulse-IoT-Center/2.0.1/pulse-api/GUID-1EC87C8E-B411-44CD-A32D-131536E08813.html) [[PDF](https://docs.vmware.com/en/VMware-Pulse-IoT-Center/2.0.1/pulse-api.pdf)]
- [Pulse IoT Center Edge Gateways HCL](https://www.vmware.com/resources/compatibility/vcl/edgeiot.php)
- [Raspbian Docs](https://www.raspberrypi.org/documentation/raspbian/)
- [Crontab Guru](https://crontab.guru)
- [Sibling packages imports](https://stackoverflow.com/questions/6323860)
- [IOTKEN](http://iotken.com)
- [Using the IoTC Agent SDK](https://docs.vmware.com/en/VMware-Pulse-IoT-Center/2.0.1/iotc-user-guide/GUID-0AA2EE47-C476-4D96-A3B8-C736C8728936.html)