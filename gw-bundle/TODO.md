# Check RPi Version
```bash
cat /etc/os-release
uname -a
lsb_release -a
cat /etc/debian_version
cat /proc/cpuinfo, model list: https://elinux.org/RPi_HardwareHistory
cat /proc/device-tree/model
```

# Set hostname
Command: `hostnamectl` 

Params:
 - set-hostname – actually configuring the hostname (you don't need to know config file locations, just enter the hostname)
 - set-chassis – easy way of confirming the hardware your OS is running on (and whether it's a VM or not)
 - set-deployment – option to specify if your Linux box is development, staging or production
 - set-location – allows you to specify geographical location of the server (freeform text)

Example: `sudo hostnamectl set-hostname iot-box-0`