#!/opt/myiot/venv/bin/python

import os
import logging
import datetime
from shutil import rmtree
from argparse import ArgumentParser
from picamera import PiCamera
from pulse.iotc import MAX_UPLOAD_SIZE, get_device_enrollment, upload_file

logging.basicConfig(level=logging.INFO, format='%(asctime)s [%(name)-12.12s] [%(levelname)-5.5s]  %(message)s')


def setup_args():
    parser = ArgumentParser()

    parser.add_argument('-m', dest='mode', help='Camera operation mode', choices=['photo', 'video', 'cleanup'], default='photo')
    parser.add_argument('-d', dest='outdir', help='Output directory', default='/tmp/camera')
    parser.add_argument('-r', dest='resolution', help='Output resolution', default='1024,768')
    parser.add_argument('-q', dest='quality', help='Image/video quality', choices=['low', 'medium', 'high'], default='low')
    parser.add_argument('-rot', dest='rotate', choices=['0', '90', '180', '270'], help='Rotate image')

    return parser.parse_args()


def get_timestamp():
    d = datetime.datetime.now()
    return f'{d.year}{d.month:02d}{d.day:02d}{d.hour:02d}{d.minute:02d}{d.second:02d}'


def make_photo(outdir, size, q, r):
    fn = f'{outdir}/photo-{get_timestamp()}.jpg'
    camera = PiCamera()
    camera.rotation = r
    camera.capture(fn, format='jpeg', resize=size, quality=q)

    return fn


def record_video(outdir, size, q, r, length):
    fn = f'{outdir}/video-{get_timestamp()}.h264'
    camera = PiCamera()
    camera.rotation = r
    camera.start_recording(fn, format='h264', resize=size, quality=q)
    camera.wait_recording(length)
    camera.stop_recording()

    return fn


def main():
    args = setup_args()

    if not os.path.exists(args.outdir):
        os.makedirs(args.outdir, exist_ok=True)

    media_file = None

    if args.mode == 'photo':
        q = 1
        if args.quality == 'low':
            q = 50
        elif args.quality == 'medium':
            q = 75
        elif args.quality == 'high':
            q = 95

        r = 0
        if args.rotate:
            r = int(args.rotate)

        media_file = make_photo(args.outdir, eval(args.resolution), q, r)

    elif args.mode == 'video':
        q = 1
        if args.quality == 'low':
            q = 35
        elif args.quality == 'medium':
            q = 20
        elif args.quality == 'high':
            q = 10

        r = 0
        if args.rotate:
            r = int(args.rotate)

        media_file = record_video(args.outdir, eval(args.resolution), q, r, 10)

    elif args.mode == 'cleanup':
        rmtree(args.outdir)

    if media_file:
        logging.info(f'Captured media file to {media_file}')
        if os.path.getsize(media_file) < MAX_UPLOAD_SIZE:
            device_id, device_token = get_device_enrollment()
            filename = os.path.basename(media_file)
            upload_file(media_file, filename, device_id, device_token)
        else:
            logging.warning(f'Could not upload file bigger than {MAX_UPLOAD_SIZE} bytes')


if __name__ == '__main__':
    main()
