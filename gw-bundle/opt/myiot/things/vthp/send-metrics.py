#!/opt/myiot/venv/bin/python

import math
import logging
import board
import busio
from adafruit_sht31d import SHT31D
from adafruit_bme680 import Adafruit_BME680_I2C
from pulse.settings import APP_ID, THING_VTHP_ID
from pulse.client import open_session, close_session, send_metric


def get_dew_point(temp, hum):
    b = 17.62
    c = 243.12
    gamma = (b * temp / (c + temp)) + math.log(hum / 100.0)
    dew_point = (c * gamma) / (b - gamma)

    return round(dew_point, 2)


def main():
    i2c = busio.I2C(board.SCL, board.SDA)

    sht31d = SHT31D(i2c)
    bme680 = Adafruit_BME680_I2C(i2c)

    temp1 = sht31d.temperature
    hum1 = sht31d.relative_humidity

    temp2 = bme680.temperature
    hum2 = bme680.humidity
    press2 = bme680.pressure

    avg_temp = round((temp1 + temp2) / 2, 2)
    avg_hum = round((hum1 + hum2) / 2)
    avg_press = round(press2  * 100 / 133)  # convert to mmHg

    dew_point = get_dew_point(avg_temp, avg_hum)

    session = open_session(APP_ID)

    result = send_metric(session, THING_VTHP_ID, 'Temperature', avg_temp)
    logging.info(f'Sent metric Temperature = {avg_temp} with result = {result}')

    result = send_metric(session, THING_VTHP_ID, 'Humidity', avg_hum)
    logging.info(f'Sent metric Humidity = {avg_hum} with result = {result}')

    result = send_metric(session, THING_VTHP_ID, 'Barometric Pressure', avg_press)
    logging.info(f'Sent metric Barometric Pressure = {avg_press} with result = {result}')

    result = send_metric(session, THING_VTHP_ID, 'Dew Point', dew_point)
    logging.info(f'Sent metric Dew Point = {dew_point} with result = {result}')

    close_session(session)


if __name__ == '__main__':
    main()
