#!/opt/myiot/venv/bin/python

import logging
from bh1745 import BH1745
from pulse.settings import APP_ID, THING_BH1745_ID
from pulse.client import open_session, close_session, send_metric


def main():
    sensor = BH1745()
    sensor.setup()

    session = open_session(APP_ID)

    rgb = sensor.get_rgb_scaled()

    result = send_metric(session, THING_BH1745_ID, 'Red Color', rgb[0])
    logging.info(f'Sent metric Red = {rgb[0]} with result = {result}')

    result = send_metric(session, THING_BH1745_ID, 'Green Color', rgb[1])
    logging.info(f'Sent metric Green = {rgb[1]} with result = {result}')

    result = send_metric(session, THING_BH1745_ID, 'Blue Color', rgb[2])
    logging.info(f'Sent metric Blue = {rgb[2]} with result = {result}')

    close_session(session)


if __name__ == '__main__':
    main()
