#!/opt/myiot/venv/bin/python

import logging
from board import SCL, SDA
from busio import I2C
from argparse import ArgumentParser, ArgumentTypeError
from adafruit_tcs34725 import TCS34725

logging.basicConfig(level=logging.INFO, format='%(asctime)s [%(name)-12.12s] [%(levelname)-5.5s]  %(message)s')

TIME_MIN = 2.4
TIME_MAX = 614.4


def time_range(arg):
    """Type function for argparse - a float within some predefined bounds"""
    try:
        f = float(arg)
    except ValueError:
        raise ArgumentTypeError('must be a floating point number')
    if f < TIME_MIN or f > TIME_MAX:
        raise ArgumentTypeError(f'must be between {str(TIME_MIN)} and {str(TIME_MAX)}')
    return f


def setup_args():
    parser = ArgumentParser()

    parser.add_argument('-t', dest='time', help=f'Set Integration Time in milliseconds between {TIME_MIN} and {TIME_MAX}',
                        type=time_range, metavar=f'{{{TIME_MIN}-{TIME_MAX}}}', required=False)
    parser.add_argument('-g', dest='gain', help='Set Gain', type=int, choices=[1, 4, 16, 60], required=False)
    parser.add_argument('-a', dest='attenuation', help='Set Glass Attenuation', type=float, required=False)

    return parser.parse_args()


def main():
    args = setup_args()

    i2c = I2C(SCL, SDA)
    sensor = TCS34725(i2c)

    sensor.integration_time = args.time if args.time else 2.4
    sensor.gain = args.gain if args.gain else 1
    sensor.glass_attenuation = args.attenuation if args.attenuation else 1.0
    logging.info(f'Set Integration Time = {args.time}')
    logging.info(f'Set Gain = {args.gain}')
    logging.info(f'Set Glass Attenuation = {args.attenuation}')


if __name__ == '__main__':
    main()

