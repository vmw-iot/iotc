#!/opt/myiot/venv/bin/python

import logging
from pulse.settings import APP_ID
from pulse.client import open_session, close_session, get_devices


def main():
    session = open_session(APP_ID)

    devices = get_devices(session, None)
    device_id = ''
    for dev in devices:
        device_type = 'UNKNOWN'
        if dev["type"] == 1:
            device_type = 'GATEWAY'
        elif dev["type"] == 2:
            device_type = 'THING'
        logging.info(f'Found device {dev["id"]} with type {device_type}')

    close_session(session)


if __name__ == '__main__':
    main()
