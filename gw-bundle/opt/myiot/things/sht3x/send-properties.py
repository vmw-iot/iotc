#!/opt/myiot/venv/bin/python

import logging
import board
import busio
import adafruit_sht31d
from pulse.settings import APP_ID, THING_SHT31_ID
from pulse.client import open_session, close_session, send_properties


def main():
    i2c = busio.I2C(board.SCL, board.SDA)
    sensor = adafruit_sht31d.SHT31D(i2c)

    session = open_session(APP_ID)

    properties = {
        'i2c-address': hex(sensor.i2c_device.device_address),
        'status': str(sensor.status),
        'serial-number': str(sensor.serial_number)
    }

    result = send_properties(session, THING_SHT31_ID, properties)
    logging.info(f'Sent property with result = {result}')

    close_session(session)


if __name__ == '__main__':
    main()
