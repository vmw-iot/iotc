#!/opt/myiot/venv/bin/python

import logging
import board
import busio
import adafruit_bme680
from pulse.settings import APP_ID, THING_BME680_ID
from pulse.client import open_session, close_session, send_metric


def main():
    i2c = busio.I2C(board.SCL, board.SDA)
    sensor = adafruit_bme680.Adafruit_BME680_I2C(i2c, debug=False)

    # sensor.sea_level_pressure = 994.58  # Moscow

    session = open_session(APP_ID)

    temp = round(sensor.temperature, 2)
    result = send_metric(session, THING_BME680_ID, 'Temperature', temp)
    logging.info(f'Sent metric Temperature = {temp} with result = {result}')

    hum = round(sensor.humidity)
    result = send_metric(session, THING_BME680_ID, 'Humidity', hum)
    logging.info(f'Sent metric Humidity = {hum} with result = {result}')

    press = round(sensor.pressure * 100 / 133)  # convert from hPa to mmHg
    result = send_metric(session, THING_BME680_ID, 'Pressure', press)
    logging.info(f'Sent metric Pressure = {press} with result = {result}')

    result = send_metric(session, THING_BME680_ID, 'Gas', sensor.gas)
    logging.info(f'Sent metric Gas = {sensor.gas} with result = {result}')

    # alt = round(sensor.altitude)
    # result = send_metric(session, THING_BME680_ID, 'Altitude', alt)
    # logging.info(f'Sent metric Altitude = {alt} with result = {result}')

    close_session(session)


if __name__ == '__main__':
    main()
