#!/opt/myiot/venv/bin/python

import logging
import random
from pulse.settings import APP_ID, THING_VIRTUAL_ID
from pulse.client import open_session, close_session, send_metric


def main():
    session = open_session(APP_ID)
    result = send_metric(session, THING_VIRTUAL_ID, 'duo', random.randint(0, 10))
    logging.info(f'Sent metric with result = {result}')

    result = send_metric(session, THING_VIRTUAL_ID, 'tst', random.randint(0, 1000))
    logging.info(f'Sent metric with result = {result}')
    close_session(session)


if __name__ == '__main__':
    main()
