#!/opt/myiot/venv/bin/python

import logging
from argparse import ArgumentParser
from bh1745 import BH1745

logging.basicConfig(level=logging.INFO, format='%(asctime)s [%(name)-12.12s] [%(levelname)-5.5s]  %(message)s')


def setup_args():
    parser = ArgumentParser()

    parser.add_argument('-led', dest='led', help='Set LED state', choices=['on', 'off'])
    parser.add_argument('-wb', dest='wb', help='Set scale mode to adjust white balance', action='store_true')
    parser.add_argument('-rgbc', dest='rgbc', help='R,G,B,C values for adjusting white balance', default='2.2,1.0,1.8,10.0')

    return parser.parse_args()


def main():
    args = setup_args()

    sensor = BH1745()
    sensor.setup()

    if args.led == 'on':
        sensor.set_leds(1)
        logging.info(f'LED Enabled')
    elif args.led == 'off':
        sensor.set_leds(0)
        logging.info(f'LED Disabled')

    if args.rgbc:
        r, g, b, c = eval(args.rgbc)
        sensor.set_channel_compensation(r, g, b, c)
        logging.info(f'Channel compensation set to R={r}, G={g}, B={b}, C={c}')

    if args.wb:
        sensor.enable_white_balance(True)
        logging.info(f'White balance compensation enabled')


if __name__ == '__main__':
    main()

