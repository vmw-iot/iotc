#!/opt/myiot/venv/bin/python

import logging
import board
import busio
import adafruit_sht31d
from pulse.settings import APP_ID, THING_SHT31_ID
from pulse.client import open_session, close_session, send_metric


def main():
    i2c = busio.I2C(board.SCL, board.SDA)
    sensor = adafruit_sht31d.SHT31D(i2c)

    session = open_session(APP_ID)

    temp = round(sensor.temperature, 2)
    result = send_metric(session, THING_SHT31_ID, 'Temperature', temp)
    logging.info(f'Sent metric Temperature = {temp} with result = {result}')

    hum = round(sensor.relative_humidity)
    result = send_metric(session, THING_SHT31_ID, 'Humidity', hum)
    logging.info(f'Sent metric Humidity = {hum} with result = {result}')

    close_session(session)


if __name__ == '__main__':
    main()
